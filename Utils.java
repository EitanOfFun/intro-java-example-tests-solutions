import java.util.Arrays;

public class Utils {

    public static String toString(int[][] a) {
        String s = "\n{";
        for (int i = 0; i < a.length; i++) {
            s += Arrays.toString(a[i]) + ",\n";
        }
        return s.subSequence(0, s.length() - 2) + "}";
    }
}
