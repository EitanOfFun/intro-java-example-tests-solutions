public class IntList {
    private IntNode head;

    public IntList() {
        head = null;
    }

    private static void reverse(IntList a) {
        IntNode rev, curr;
        rev = null;
        curr = a.head;
        while(curr != null) {
            a.head = a.head.getNext();
            curr.setNext(rev);
            rev = curr;
            curr = a.head;
        }
        a.head = rev;
    }
    public static IntList createIntList(int... xs) {
        IntList a = new IntList();
        for (int x : xs) {
            a.head = new IntNode(x, a.head);
        }
        IntList.reverse(a);
        return a;
    }
    public String toString() {
        reverse(this);
        String s = "";
        for (IntNode p = head; p != null; p = p.getNext()) {
            s = p.getNum() + s;
        }
        reverse(this);
        return s;
    }

    // 2014a

    /**
     * Q3
     */
    public void sumPairs() {
        IntNode p = head;

        while (p != null) {
            if (p.getNext() != null) {
                p.setNum(p.getNum() + p.getNext().getNum());
                p.setNext(p.getNext().getNext());
            }
            p = p.getNext();
        }
    }
}

