
public class TestQ2 {
    public static boolean solution(boolean[][] maze) {
        boolean[][] wasHere = new boolean[maze.length][maze[0].length];
        return solution(maze, wasHere, 0, 0);
    }

    private static boolean solution(boolean[][] m, boolean wasHere[][], int i, int j) {
        if (i == m.length - 1 && j == m[0].length - 1)
            return m[i][j];

        if (i < 0 || i == m.length || j < 0 || j == m[0].length || !m[i][j] || wasHere[i][j])
            return false;
        wasHere[i][j] = true;

        return  solution(m, wasHere, i + 1, j) ||
                solution(m, wasHere, i - 1, j) ||
                solution(m, wasHere, i, j  + 1) ||
                solution(m, wasHere, i, j - 1);
    }
}
