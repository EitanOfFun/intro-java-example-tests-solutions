
public class TestQ1 {
    public static void distance(int[] a) {
        int counter = 0;
        for (int i = 0; i < a.length; i++) {
            if (a[i] != 0)
                a[i] = ++counter;
            else
                counter = 0;
        }
        counter = 0;
        for (int i = a.length - 1; i >= 0 ; i--) {
            if (a[i] != 0 && a[i] > counter + 1)
                a[i] = ++counter;
            else
                counter = 0;

        }
    }
}
