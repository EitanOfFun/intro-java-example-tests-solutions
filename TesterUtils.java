
public class TesterUtils {
    public static void createNumMaze(boolean[][] maze, int[][] m2, int i, int j) {
        if (i == maze.length)
            return;
        if (j == maze.length) {
            createNumMaze(maze, m2, i + 1, 0);
        }
        else {
            if (maze[i][j])
                m2[i][j] = 1;
            else {
                m2[i][j] = 0;
            }
            createNumMaze(maze, m2, i, j + 1);
        }
    }

}
